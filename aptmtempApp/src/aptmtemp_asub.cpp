#include <stdio.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <epicsTypes.h>
#include <cmath>
#include <cstring>
#include <vector>

//------------------------------------------------------------------------
//  Initialize buffers with zero
//------------------------------------------------------------------------
static long inittemprecorder(aSubRecord *prec)
{
    int nelm = *(int *)prec->d;
    memset(prec->vala, 0, nelm*(sizeof(float)));    
    return 0;

    *(int *)prec->valb = 0;
    prec->nevb = 1;
}

//------------------------------------------------------------------------
//  Create the circular buffer
//------------------------------------------------------------------------
static long temprecorder(aSubRecord *prec)
{
    float value = *(float *)prec->a;        // Sample value
    int writePtr = *(int *)prec->b;         //Write pointer
    int nelm = *(int *)prec->c;             //Num elements

    // Insert new point
    float *pBuffer = (float *)prec->vala;
    pBuffer[writePtr] = value;
    prec->neva = nelm;
    
    // Update write pointer
    writePtr = (writePtr == (nelm-1) ? 0 : (writePtr + 1));
    *(int *)prec->valb = writePtr;
    prec->nevb = 1;

    return 0;
}

//------------------------------------------------------------------------
//  Capture the ring buffer
//------------------------------------------------------------------------
static long generateWave(aSubRecord *prec)
{
    float *pInputBuffer = (float *)prec->a; // unordered buffer
    int writePtr = *(int *)prec->b;         //Write pointer
    int nelm = *(int *)prec->c;             //Num elements
    float *pOutBuffer = (float *)prec->vala; // Output array

    int i = 0;
    int ii = writePtr;
    
    // Obtain ordered waveform
    while (i < nelm) {
        pOutBuffer[i] = pInputBuffer[ii];
        ii = (ii == (nelm-1) ? 0 : (ii + 1));
        ++i;
    }
    prec->neva = nelm;

    return 0;
}

extern "C"
{
    epicsRegisterFunction(temprecorder);
    epicsRegisterFunction(inittemprecorder);
    epicsRegisterFunction(generateWave);
}
