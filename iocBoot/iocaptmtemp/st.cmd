#!../../bin/darwin-x86/aptmtemp

#- You may have to change aptmtemp to something else
#- everywhere it appears in this file

< envPaths

## Register all support components
dbLoadDatabase("../../dbd/aptmtemp.dbd",0,0)
aptmtemp_registerRecordDeviceDriver(pdbbase) 

#- Macros
epicsEnvSet(PREFIX, "aptm")

## Load record instances
dbLoadRecords("../../db/simulation.template","P=$(PREFIX):, R=")
dbLoadRecords("../../db/temprecorder.template","P=$(PREFIX):, R=, NELM=20")

iocInit()

